Hacker News Library project
This project includes android native library and correspoding Cordova plugin. The library/plugin allows you to fetch top 20 new stories from Hacker News API and load the news into DialogFragment. Each news can be clicked to view fully in browser. The library/plugin uses Retrofit framework, make sure your project support it.


Android Native Setup:

	1. Download and place hackernewssdk-release.aar file into your app libs directory

	2. Follow Google's guide(Add your library as a dependency, https://developer.android.com/studio/projects/android-library.html#AddDependency) to add library. In step 3, use following :
	
		dependencies {
		    compile 'com.squareup.retrofit2:retrofit:2.0.0-SNAPSHOT'
	    	compile 'com.squareup.retrofit2:converter-gson:2.1.0'
	    	compile (name: 'hackernewssdk-release', ext:'aar')
		}
	Maybe you need to update your gradle or Java version, because retrofit requires at minimum Java 7 or Android 2.3. 

	Also, add following repositories into your app module's build.gradle
		repositories {
	    	flatDir {
	        	dirs 'libs'
	    	}
		}

	3. The package name is com.yuao.mobile.hackernewssdk, make sure you import it before use

Cordova/Phonegap Setup:
	
	1. Using following command to install plugin under your project directory
		cordova plugin add https://yuaoliu@bitbucket.org/yuaoliu/android.git


How to use

Android Native:
	There are two static methods on the HN class, as follows:
	public static void initSDK();
	public static void showHackerNews(Activity context) {

Cordova/Phonegap:
	There are two static methods on the window.CordovaHN object, as follows:
	window.CordovaHN.initSDK();
	window.CordovaHN.showHackerNews();