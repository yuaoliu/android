package com.yuao.cordova.HNplugin;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.yuao.mobile.hackernewssdk.*;

/**
* This class show news called from JavaScript.
*/
public class CordovaHN extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("showHackerNews")) {
        	cordova.getActivity().runOnUiThread(new Runnable() {
            	public void run() {
            		showHackerNews();
            	}
        	});
        	return true;
        } else if (action.equals("initSDK")) {
            init();
            return true;
        }
        return false;
    }

    private void showHackerNews() {
    	HN.showHackerNews(cordova.getActivity());
    }

    private void init() {
        HN.initSDK();
    }

}